const IndesitIWUD4085 = { name: "Стиральная машина Indesit IWUD 4085 (CIS)", price: "14499", volume: "4", dataId: "1" },
	CandyCS41051D1 = { name: "Стиральная машина Candy CS4 1051D1/2-07", price: "14599", volume: "5", dataId: "2" },
	IndesitIWUB4085 = { name: "Стиральная машина Indesit IWUB 4085 (CIS)", price: "14699", volume: "4", dataId: "3" },
	IndesitIWUB4105 = { name: "Стиральная машина Indesit IWUB 4105 (CIS)", price: "14799", volume: "4", dataId: "4" },
	BekoWRS54P1BSW = { name: "Стиральная машина Beko WRS 54P1 BSW", price: "14999", volume: "5", dataId: "5" },
	DEXPWMF510DVL = { name: "Стиральная машина DEXP WM-F510DVL/WW", price: "14999", volume: "5", dataId: "6" },
	CandyCS341052D1 = { name: "Стиральная машина Candy CS34 1052D1/2-07", price: "15399", volume: "5", dataId: "7" },
	BekoWDN635P2BSW = { name: "Стиральная машина Beko WDN 635P2 BSW", price: "15499", volume: "6", dataId: "8" },
	ATLANTСМА50 = { name: "Стиральная машина ATLANT СМА-50 У 101-00", price: "15799", volume: "5", dataId: "9" },
	HotpointAristonVMSL501B = { name: "Стиральная машина Hotpoint-Ariston VMSL 501 B", price: "16399", volume: "5", dataId: "10" };
const washingMachines = [ IndesitIWUD4085, CandyCS41051D1, IndesitIWUB4085, IndesitIWUB4105, 
						BekoWRS54P1BSW, DEXPWMF510DVL, CandyCS341052D1, BekoWDN635P2BSW, 
						ATLANTСМА50, HotpointAristonVMSL501B ];

export { washingMachines }; 